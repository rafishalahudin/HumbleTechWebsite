<?php
    include 'koneksi/koneksi.php';
    include 'lib/lib.php';
//    session_start();
    $username = $_SESSION['username'];  
    cekLogin();
?>

    <!DOCTYPE html>
    <html lang="en">

    <head>

        <title>Humble Tech - Admin</title>

        <link href="lib/css/bootstrap.min.css" rel="stylesheet">
        <link href="lib/css/sb-admin.css" rel="stylesheet">
        <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <style>
            .panel {
                width: 500px;
            }

        </style>
    </head>

    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <img src="lib/image/logohumblewhite.png" width="150px" height="50px">
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">


                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?= $username ?><b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#simu"><i class="fa fa-fw fa-table"></i> Database Simulasi <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="simu" class="collapse">
                                <li>
                                    <a href="Processor/dataproc.php">Processor</a>
                                </li>
                                <li>
                                    <a href="Motherboard/datamobo.php">Motherboard</a>
                                </li>
                                <li>
                                    <a href="Vga/datavga.php">VGA</a>
                                </li>
                                <li>
                                    <a href="Ram/dataram.php">RAM</a>
                                </li>
                                <li>
                                    <a href="Harddisk/datahdd.php">Hard Disk Drive</a>
                                </li>
                                <li>
                                    <a href="Powersuply/datapsu.php">Power Supply</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#art"><i class="fa fa-fw fa-table"></i> Database Artikel <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="art" class="collapse">
                                <li>
                                    <a href="ArtikelProc/datakonten.php">Artikel Processor</a>
                                </li>
                                <li>
                                    <a href="Event/dataevent.php">Artikel Event</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-table"></i> Form Simulasi <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="demo" class="collapse">
                                <li>
                                    <a href="Processor/tambahproc.php">Processor</a>
                                </li>
                                <li>
                                    <a href="Motherboard/tambahmobo.php">Motherboard</a>
                                </li>
                                <li>
                                    <a href="Vga/tambahvga.php">VGA</a>
                                </li>
                                <li>
                                    <a href="Ram/tambahram.php">RAM</a>
                                </li>
                                <li>
                                    <a href="Harddisk/tambahhdd.php">Hard Disk Drive</a>
                                </li>
                                <li>
                                    <a href="Powersuply/tambahpsu.php">Power Supply</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#ha"><i class="fa fa-fw fa-arrows-v"></i> Form Artikel <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="ha" class="collapse">
                                <li>
                                    <a href="simulasi/datasimulasi.php">Simulasi</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>


            <div id="page-wrapper">

                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="page-header">
                                <h1><span class="glyphicon glyphicon-th-list"> </span> Dashboard Admin <small>Statistik</small></h1>
                            </div>
                            <div class="col-sm-6">
                                <div class="panel panel-primary ">
                                    <div class="panel-heading " style="background-color:#12143a"><span class="glyphicon glyphicon-send"></span> Event yang baru diterbitkan</div>
                                    <?php
            $ambil = mysql_query("select * from t_event order by id_event desc limit 3");
            while($row = mysql_fetch_object($ambil)){
    ?>
                                        <div class="panel-body ">
                                            <div class="col-sm-4">
                                                <img src="Event/images/<?php echo $row->gambar?>" width="100px" height="80px">
                                            </div>
                                            <div class="col-sm-4"><b><?php echo $row->nama_event ?></b></div>
                                            <div class="col-sm-4">Di post tanggal :
                                                <?php echo $row->tanggal?>
                                            </div>

                                        </div>
                                        <?php
            }
                            ?>
                                </div>
                            </div>

                            <div class="col-sm-6">

                                <div class="panel panel-primary">
                                    <div class="panel-heading" style="background-color:#12143a"><span class="glyphicon glyphicon-send"></span> Berita yang baru diterbitkan</div>
                                    <?php
            $ambil = mysql_query("select * from t_artikel_proc order by id_artikel_proc desc limit 3");
            while($row = mysql_fetch_object($ambil)){
    ?>
                                        <div class="panel-body ">
                                            <div class="col-sm-4">
                                                <img src="ArtikelProc/images/<?php echo $row->gambar?>" width="100px" height="80px">
                                            </div>
                                            <div class="col-sm-4"><b><?php echo $row->nama_judul ?></b></div>
                                            <div class="col-sm-4">Di post tanggal :
                                                <?php echo $row->tanggal?>
                                            </div>

                                        </div>
                                        <?php
            }
                            ?>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- /.row -->

                </div>
            </div>
        </div>
        <!-- jQuery -->
        <script src="lib/js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="lib/js/bootstrap.min.js"></script>

        <!-- Morris Charts JavaScript -->
        <script src="lib/js/plugins/morris/raphael.min.js"></script>
        <script src="lib/js/plugins/morris/morris.min.js"></script>
        <script src="lib/js/plugins/morris/morris-data.js"></script>

    </body>

    </html>
