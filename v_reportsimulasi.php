<?php  
include "koneksi/koneksi.php";  

?>

    <html>

    <head>
        <title>The Humble Tech-Simulasi</title>
        <link rel="stylesheet" type="text/css" href="lib/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="lib/css/template.css">
        <style>
            h1 {
                margin-top: 100px;
            }
            
            .modal {
                z-index: 9999999 !important;
            }

        </style>
    </head>

    <body>

        <div class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"></h4>
                    </div>

                    <div class="modal-body">
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary btnYa">Ya</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Tidak</button>
                    </div>
                </div>
            </div>
        </div>

        <nav id="mainNav" class="navbar navbar-fixed-top navar">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                    </button>
                    <img src="lib/image/logohumblewhite.png" width="150px" height="50px">
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a class="page-scroll navigasi" href="index.php">Home</a>
                        </li>
                        <li>
                            <a class="page-scroll navigasi" href="ArtikelProc/category1.php">Laman berita</a>
                        </li>
                        <li>
                            <a class="page-scroll navigasi" href="Event/categoryevent.php">Laman Event</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>
        <div class="container">
            <div class="row">
                <center>
                    <h1>Simulasi rakit PC</h1>
                <p>Last Update : <?= date("l, j  F Y , h:i:s A") ?></p></center>
                <br>
                <div class="panel panel-default">
                    <div class="panel-heading" style="background-color: #12143a; color:white">Anda akan mensimulasikan rakitan PC sebagai berikut :</div>
                    <div class="panel-body">
                        <table class="table table-hover" style="background-color:white;">
                            <thead style="text-align:center;">
                                <th style="width: 50px;"><b>Komponen</b></th>
                                <th style="width: 800px;"><b>Merek</b></th>
                                <th style="width: 200px;"><b>Harga</b></th>
                            </thead>
                            <tbody>

                                <?php while($result = mysql_fetch_object($data)) :
                            $hpro = $result->hargapro;
                            $hram = $result->hargaram;
                            $hvga = $result->hargavga;
                            $hhdd = $result->hargahdd;
                            $hpsu = $result->hargapsu;
                            $hmobo = $result->hargamobo;
                            $htot = $hpro + $hram + $hvga + $hhdd + $hpsu +$hmobo;?>
                                    <tr>
                                        <td>
                                            Processor
                                        </td>
                                        <td>

                                            <?= $result->nama_processor?>
                                        </td>
                                        <td>Rp.
                                            <?=$result->hargapro?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            RAM
                                        </td>
                                        <td>
                                            <?= $result->nama_ram?>
                                        </td>
                                        <td>Rp.
                                            <?=$result->hargaram?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            VGA
                                        </td>
                                        <td>
                                            <?= $result->nama_vga?>
                                        </td>
                                        <td>Rp.
                                            <?=$result->hargavga?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Motherboard
                                        </td>
                                        <td>
                                            <?= $result->nama_mobo?>
                                        </td>
                                        <td>Rp.
                                            <?=$result->hargamobo?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Power Supply
                                        </td>
                                        <td>
                                            <?= $result->nama_psu?>
                                        </td>
                                        <td>Rp.
                                            <?=$result->hargapsu?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Hard Disk
                                        </td>
                                        <td>
                                            <?= $result->nama_hdd?>
                                        </td>
                                        <td>Rp.
                                            <?=$result->hargahdd?>
                                        </td>

                                    </tr>
                                    <tr style="font-weight:bold;">
                                        <td></td>
                                        <td>Jumlah:</td>
                                        <td >Rp.
                                            <?=$htot ?>
                                        </td>
                                    </tr>
                                    <?php endwhile;?>
                            </tbody>
                        </table>
                            <a class="button" href="index.php">Kembali ke halaman beranda</a>
                    </div></div>

                    </div>
                </div>

                <script src="lib/media/js/jquery.min.js"></script>
                <script src="lib/js/bootstrap.min.js"></script>
                <script src="lib/media/plugins/toastr/toastr.min.js"></script>
                <link href="lib/media/plugins/toastr/toastr.min.css" rel="stylesheet">
                <script type="text/javascript">
                    $(function() {
                        $(".btnDelete").on("click", function(e) {
                            e.preventDefault();

                            var nama = $(this).parent().parent().children()[1];
                            nama = $(nama).html();
                            var tr = $(this).parent().parent();

                            $(".modal").modal('show');
                            $(".modal-title").html("Konfirmasi");
                            $(".modal-body").html('Anda yakin ingin menghapus data<b>' + nama + '</b> ?');

                            var href = $(this).attr('href');

                            $('.btnYa').off();
                            $('.btnYa').on('click', function() {

                                $.ajax({
                                    'url': href,
                                    'type': "POST",
                                    'success': function(result) {


                                        if (result == 1) {
                                            $(".modal").modal('hide');
                                            tr.fadeOut();

                                            toastr.success('Data berhasil dihapus', 'Informasi');
                                        }
                                    }
                                });
                            });
                        });
                    });

                </script>
    </body>

    </html>

