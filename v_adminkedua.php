<?php
    include 'lib/lib.php';
//    session_start();
    $username = $_SESSION['username'];  
    cekLogin();
?>

    <!DOCTYPE html>
    <html lang="en">

    <head>

        <title>Humble Tech - Admin</title>

        <link href="lib/css/bootstrap.min.css" rel="stylesheet">
        <link href="lib/css/sb-admin.css" rel="stylesheet">
        <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    </head>

    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <img src="lib/image/logohumblewhite.png" width="150px" height="50px">
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">


                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?= $username ?><b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">

                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#art"><i class="fa fa-fw fa-table"></i> Database Artikel <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="art" class="collapse">
                                <li>
                                    <a href="ArtikelProc/datakonten.php">Artikel Berita</a>
                                </li>
                                <li>
                                    <a href="Event/dataevent.php">Artikel Event</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:;" data-toggle="collapse" data-target="#ha"><i class="fa fa-fw fa-arrows-v"></i> Tambah Artikel <i class="fa fa-fw fa-caret-down"></i></a>
                            <ul id="ha" class="collapse">
                                <li>
                                    <a href="ArtikelProc/tambahkonten.php">Tambah Artikel Berita</a>
                                </li>
                                <li>
                                    <a href="Event/tambahevent.php">Tambah Artikel Events</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>


            <div id="page-wrapper">

                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="row">
                        <div class="page-header">
                            <h1><span class="glyphicon glyphicon-th-list"> </span> Dashboard Admin 2<small>Statistik</small></h1>
                        </div>
                        <div class="col-sm-8">
                            <div class="panel panel-primary ">
                                <div class="panel-heading " style="background-color:#12143a"><span class="glyphicon glyphicon-send"></span> Saran dari pengguna terhadap web</div>

                                <div class="panel-body ">
                                    <table class="table">
                                        <tr>
                                            <td>Nama</td>
                                            <td>Telepon</td>
                                            <td>Email</td>
                                            <td>Alamat</td>
                                            <td>Pesan</td>
                                        </tr>
                                        <?php
   
            include 'koneksi/koneksi.php';
            $sql = "SELECT * FROM t_feedback";
            $data = mysql_query($sql) or die(mysql_error());
            while($row = mysql_fetch_object($data)){
                
    ?>
                                            <tr>
                                                <td>
                                                    <?php echo $row->nama ?>
                                                </td>
                                                <td>
                                                    <?php echo $row->phone?>
                                                </td>
                                                <td>
                                                    <?php echo $row->email?>
                                                </td>
                                                <td>
                                                    <?php echo $row->alamat?>
                                                </td>
                                                <td>
                                                    <?php echo $row->pesan?>
                                                </td>
                                            </tr>
                                            <?php
            }
                            ?>
                                    </table>
                                </div>

                            </div>

                        </div>
                    </div>
                    <!-- /.row -->

                </div>
            </div>
        </div>
        <!-- jQuery -->
        <script src="lib/js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="lib/js/bootstrap.min.js"></script>

        <!-- Morris Charts JavaScript -->
        <script src="lib/js/plugins/morris/raphael.min.js"></script>
        <script src="lib/js/plugins/morris/morris.min.js"></script>
        <script src="lib/js/plugins/morris/morris-data.js"></script>

    </body>

    </html>
