<html>

<head>

    <link rel="stylesheet" type="text/css" href="lib/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="lib/css/login.css">
    <script type="text/javascript" src="lib/js/jquery.min.js"></script>
    <script type="text/javascript" src="lib/js/bootstrap.min.js"></script>


</head>

<body>
    <div class="container">
        <div class="row">
            <img src="lib/image/logohumblewhite2.png">
            <a href="index.php" class="form-signin-heading buttona" style="text-decoration:none;margin-left:25%; ">Take me to home page</a>
            <a class="form-signin-heading buttona" style="text-decoration:none;" onclick="document.getElementById('id01').style.display='block'">Login To Admin</a>
      
        </div>
    </div>

    <div id="id01" class="modal">
        
        <div class="modal-dialog modal-lg" role="document">

            <form class="modal-content animate" action="login.php" method="post">
                <div class="imgcontainer">
                    <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal">&times;</span>
                    <img src="lib/image/logohumbleblack.png" alt="Avatar" class="avatar">
                </div>

                <form action="login.php" method="post">
                    <div class="form-group">
                        <input type="text" name="username" class="form-control" placeholder="Masukan Username" required>
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="Masukan Password" required>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Login</button>
                        
                    </div>

                    <div class="modal-footer" style="background-color:#f1f1f1">
                        <button type="button" onclick="document.getElementById('id01').style.display='none'" class="btn btn-primary" style="margin-right:178px;">Cancel</button>
                    </div>

                </form>
            </form>
            
        </div>
        
    </div>
    
    <script>
        // Get the modal
        var modal = document.getElementById('id01');

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }

    </script>

</body>

</html>
