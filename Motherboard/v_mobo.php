<!DOCTYPE html>
<html lang="en">

<head>

    <title>Humble Tech - Admin</title>

    <link href="../lib/css/bootstrap.min.css" rel="stylesheet">
    <link href="../lib/css/sb-admin.css" rel="stylesheet">
    <link href="../lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Humble Tech</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav" >


                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Rafi Shalahudin <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="../logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#simu"><i class="fa fa-fw fa-table"></i> Database Simulasi <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="simu" class="collapse">
                            <li>
                                <a href="../Processor/dataproc.php">Processor</a>
                            </li>
                            <li>
                                <a href="../Motherboard/datamobo.php">Motherboard</a>
                            </li>
                            <li>
                                <a href="../Vga/datavga.php">VGA</a>
                            </li>
                            <li>
                                <a href="../Ram/dataram.php">RAM</a>
                            </li>
                            <li>
                                <a href="../Harddisk/datahdd.php">Hard Disk Drive</a>
                            </li>
                            <li>
                                <a href="../Powersuply/datapsu.php">Power Supply</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#art"><i class="fa fa-fw fa-table"></i> Database Artikel <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="art" class="collapse">
                            <li>
                                <a href="../ArtikelProc/datakonten.php">Artikel Processor</a>
                            </li>
                            <li>
                                <a href="../Event/dataevent.php">Artikel Events</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-table"></i> Form Simulasi <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="../Processor/tambahproc.php">Processor</a>
                            </li>
                            <li>
                                <a href="../Motherboard/tambahmobo.php">Motherboard</a>
                            </li>
                            <li>
                                <a href="../Vga/tambahvga.php">VGA</a>
                            </li>
                            <li>
                                <a href="../Ram/tambahram.php">RAM</a>
                            </li>
                            <li>
                                <a href="../Harddisk/tambahhdd.php">Hard Disk Drive</a>
                            </li>
                            <li>
                                <a href="../Powersuply/tam">Power Supply</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#ha"><i class="fa fa-fw fa-arrows-v"></i> Form Artikel <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="ha" class="collapse">
                            <li>
                                <a href="../simulasi/datasimulasi.php">Simulasi</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>


        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <h1 class="page-heading">
                    Data Motherboard
                        <small> <a class="btn btn-info" style="background-color:transparent; color:black;" href="tambahmobo.php">Tambah Data</a></small>
                    </h1>
                    <div class="panel panel-default">
                        <div class="panel-heading" style="background-color: #12143a; color:white">Data Motherboard</div>
                        <div class="panel-body">
                            <table class="table" style="background-color:white;">
                                <thead>
                                    <tr style="text-align:center;font-weight: bold;">
                                        <td>ID</td>
                                        <td>Nama motherboard</td>
                                        <td>Harga Motherboard</td>
                                        <td>Action</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
            while ($result = mysql_fetch_object($data)){
        ?>

                                        <tr  style="text-align:center;">
                                            <td>
                                                <?php echo $result->id_mobo?>
                                            </td>
                                            <td>
                                                <?php echo $result->nama_mobo?>
                                            </td>
                                            <td>
                                                <?php echo $result->hargamobo ?>
                                            </td>
                                             <td>
                                                <a href="editmobo.php?id=<?php echo $result->id_mobo ?>" class="btn" style="color: black; background-color: white;border:1px solid black;"><i class="glyphicon glyphicon-pencil"></i></a>
                                                <a href="deletemobo.php?id=<?php echo $result->id_mobo ?>" class="btn btn-danger btnDelete" style="color: black; background-color: white;border:1px solid black;"><i class="glyphicon glyphicon-trash"></i></a>
                                            </td>
                                        </tr>

                                        <?php
                }
            ?>


                                            <tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal fade" tabindex="-1" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title"></h4>
                                </div>

                                <div class="modal-body">
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary btnYa">Ya</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Tidak</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <script src="../lib/media/js/jquery.min.js"></script>
                    <script src="../lib/js/bootstrap.min.js"></script>

                    <script src="../lib/media/plugins/toastr/toastr.min.js"></script>
                    <link href="../lib/media/plugins/toastr/toastr.min.css" rel="stylesheet">
                    <script type="text/javascript">
                        $(function() {
                            $(".btnDelete").on("click", function(e) {
                                e.preventDefault();

                                var nama = $(this).parent().parent().children()[1];
                                nama = $(nama).html();
                                var tr = $(this).parent().parent();

                                $(".modal").modal('show');
                                $(".modal-title").html("Konfirmasi");
                                $(".modal-body").html('Anda yakin ingin menghapus data<b>' + nama + '</b> ?');

                                var href = $(this).attr('href');

                                $('.btnYa').off();
                                $('.btnYa').on('click', function() {

                                    $.ajax({
                                        'url': href,
                                        'type': "POST",
                                        'success': function(result) {

                                            if (result == 1) {
                                                $(".modal").modal('hide');
                                                tr.fadeOut();

                                                toastr.success('Data berhasil dihapus', 'Informasi');
                                            }
                                        }
                                    });
                                });
                            });
                        });

                    </script>
                </div>
            </div>
</body>

</html>
