<!DOCTYPE html>
<html lang="en">

<head>

    <title>Humble Tech - Admin</title>

    <link href="../lib/css/bootstrap.min.css" rel="stylesheet">
    <link href="../lib/css/sb-admin.css" rel="stylesheet">
    <link href="../lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Humble Tech</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav" style="background-color: #222">


                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Rafi Shalahudin <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="../logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#simu"><i class="fa fa-fw fa-table"></i> Database Simulasi <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="simu" class="collapse">
                            <li>
                                <a href="../Processor/dataproc.php">Processor</a>
                            </li>
                            <li>
                                <a href="../Motherboard/datamobo.php">Motherboard</a>
                            </li>
                            <li>
                                <a href="../Vga/datavga.php">VGA</a>
                            </li>
                            <li>
                                <a href="../Ram/dataram.php">RAM</a>
                            </li>
                            <li>
                                <a href="../Harddisk/datahdd.php">Hard Disk Drive</a>
                            </li>
                            <li>
                                <a href="../Powersuply/datapsu.php">Power Supply</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#art"><i class="fa fa-fw fa-table"></i> Database Artikel <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="art" class="collapse">
                            <li>
                                <a href="../ArtikelProc/datakonten.php">Artikel Processor</a>
                            </li>
                            <li>
                                <a href="#">Artikel Graphic Cards</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-table"></i> Form Simulasi <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="#">Processor</a>
                            </li>
                            <li>
                                <a href="#">Motherboard</a>
                            </li>
                            <li>
                                <a href="#">VGA</a>
                            </li>
                            <li>
                                <a href="#">RAM</a>
                            </li>
                            <li>
                                <a href="#">Hard Disk Drive</a>
                            </li>
                            <li>
                                <a href="#">Power Supply</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#ha"><i class="fa fa-fw fa-arrows-v"></i> Form Artikel <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="ha" class="collapse">
                            <li>
                                <a href="#">Artikel Processor</a>
                            </li>
                            <li>
                                <a href="#">Artikel Graphic Cards</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <legend>Edit RAM</legend>

                    <div class="row">

                        <div class="col-md-8 col-md-offset-2">

                            <form action="editram.php?id=<?php echo $result->id_ram?>" method="post">

                                <fieldset>

                                    <div class="form-group col-md-6" style="color: white;">
                                        <label for="first_name">Id RAM</label>
                                        <input type="number" name="id_ram" class="form-control" maxlength="11" required  value="<?php echo $result->id_ram ?>" />
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="last_name" style="color: white;">Nama RAM</label>
                                        <input type="text" name="nama_ram" class="form-control" maxlength="100" required value="<?php echo $result->nama_ram ?>" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="last_name" style="color: white;">Harga RAM</label>
                                        <input type="text" name="hargaram" class="form-control" maxlength="100" required value="<?php echo $result->hargaram ?>" />
                                    </div>

                                </fieldset>

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <button type="submit" value="submit" class="btn btn-primary">
                                            Masukan
                                        </button>

                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>