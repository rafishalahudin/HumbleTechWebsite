$(document).ready(function () {
    var posisiheader = $('#navbar');
    var nav = $('.navbar').offset().top;
    $(window).scroll(function() {
        var posisiscroll = $(window).scrollTop();
        if (posisiscroll > nav) {
            $('.navbar').css({
                'position': 'fixed',
                'margin-top': '0px',
                'background': '#12143a',
                'color' : 'white',
                'box-shadow': '0 3px 4px 0 rgba(0, 0, 0, 0.14),0 1px 8px 0 rgba(0, 0, 0, 0.12),0 3px 3px -2px rgba(0, 0, 0, 0.4)',
                'border-bottom': '1px solid rgba(0,0,0,0.09)'
            });
            $('.navbar ul li a').css({
                'color': 'white'
            });
            $('.ikan').css({ 
                'background-image': 'url(lib/image/logohumblewhite.png)'
            });
            $('.navbar-header a').css({
               'color' : 'white'
            });
        }
        else {
            $('.navbar').css({
                'position': 'absolute',
                'margin-top': '70px',
                'background': 'transparent',
                'box-shadow': 'none',
                'border-bottom': 'none'
            });
            $('.navbar ul li a').css({
                'color': 'white'
            });
            $('.ikan').css({ 
                'background-image': 'url(lib/image/logohumblewhite.png)'
            });
            $('.navbar-header a').css({
               'color' : 'white'
            });
        }
    });
    
/*  var nav = $('.navbar').offset().top;
    $(window).scroll(function() {
        var p = $(window).scrollTop();
        var nav = $('.navbar').offset().top;
        console.log(p);
        if (p >= nav) {
            $('.navbar').css({
                'position': 'fixed',
                'top': '0',
                'background': 'white',
                'border-bottom': '1px solid rgba(0,0,0,0.09)',
                'transition': 'all 0.3s'
            });
        } else {
            $('.navbar').css({
                'position': 'fixed',
                'background': 'transparent',
                'border-bottom': '1px solid rgba(0,0,0,0.09)'
            });
        }
    });*/
});