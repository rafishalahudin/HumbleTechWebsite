$(window).scroll(function (e) {
    if ($(window).scrollTop() > 100) {
        $('.header2').css({
            'position': 'fixed',
            'top': '0',
            "background-color": 'transparent'
        });
        $('.header2 ul').css({
            "background-color": 'rgba(0,0,0,.5)'
        });
    } else {
        $('.header2').css({
            'position': '',
            "background-color": 'black'
        });
        $('.header2 ul').css({
            "background-color": 'rgba(0,0,0,1)'
        });
    }

});

$(document).ready(function() {
    $('#found_site').on('change', function() {
       $(this).val() == "other" ? $('#specify').closest('.form-group').show() : $('#specify').closest('.form-group').hide();
    })
})