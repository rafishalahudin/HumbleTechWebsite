<html>

<head>
    <title></title>

    <link rel="stylesheet" type="text/css" href="lib/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="lib/css/template.css">
    <script type="text/javascript" src="lib/js/jquery.min.js"></script>
    <script type="text/javascript" src="lib/js/bootstrap.min.js"></script>

</head>

<body>

    <nav id="mainNav" class="navbar navbar-fixed-top navar">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">Humble Tech</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll navigasi" href="index.php">Home</a>
                    </li>
                    <li>
                        <a class="page-scroll navigasi" href="#Tech-Processor">Tech-Processor</a>
                    </li>
                    <li>
                        <a class="page-scroll navigasi" href="#Tech-VGA">Tech VGA</a>
                    </li>
                    <li>
                        <a class="page-scroll navigasi" href="#Tech-MOtherboard">Tech Motherboard</a>
                    </li>
                    <li>
                        <a class="page-scroll navigasi" href="#contact" onclick="document.getElementById('id01').style.display='block'" style="width:auto;">Simulasi</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Page Content -->
    <section class="arp">
        <div class="container">

            <div class="row">

                <!-- Blog Post Content Column -->
                <div class="col-lg-8">

                    <!-- Blog Post -->

                    <!-- Title -->
                    <h1>Blog Post Title</h1>

                    <!-- Author -->
                    <p class="lead">
                        by <a href="#">Start Bootstrap</a>
                    </p>

                    <hr>

                    <!-- Date/Time -->
                    <p><span class="glyphicon glyphicon-time"></span> Posted on August 24, 2013 at 9:00 PM</p>

                    <hr>

                    <!-- Preview Image -->
                    <img class="img-responsive" src="" alt="">

                    <hr>

                    <!-- Post Content -->
                    <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus, vero, obcaecati, aut, error quam sapiente nemo saepe quibusdam sit excepturi nam quia corporis eligendi eos magni recusandae laborum minus inventore?</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut, tenetur natus doloremque laborum quos iste ipsum rerum obcaecati impedit odit illo dolorum ab tempora nihil dicta earum fugiat. Temporibus, voluptatibus.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos, doloribus, dolorem iusto blanditiis unde eius illum consequuntur neque dicta incidunt ullam ea hic porro optio ratione repellat perspiciatis. Enim, iure!</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, aliquid, animi, ut quas placeat totam sunt tempora commodi nihil ullam alias modi dicta saepe minima ab quo voluptatem obcaecati?</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, dolor quis. Sunt, ut, explicabo, aliquam tenetur ratione tempore quidem voluptates cupiditate voluptas illo saepe quaerat numquam recusandae? Qui, necessitatibus, est!</p>

                    <hr>
                </div>


                <!-- Blog Sidebar Widgets Column -->
                <div class="col-md-4">

                    <!-- Blog Search Well -->
                    <div class="well">
                        <h4>Blog Search</h4>
                        <div class="input-group">
                            <input type="text" class="form-control">
                            <span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
                            </span>
                        </div>
                        <!-- /.input-group -->
                    </div>

                    <!-- Blog Categories Well -->
                    <div class="well">
                        <h4>Blog Categories</h4>
                        <div class="row">
                            <div class="col-lg-6">
                                <ul class="list-unstyled">
                                    <li><a href="#">Category Name</a>
                                    </li>
                                    <li><a href="#">Category Name</a>
                                    </li>
                                    <li><a href="#">Category Name</a>
                                    </li>
                                    <li><a href="#">Category Name</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-lg-6">
                                <ul class="list-unstyled">
                                    <li><a href="#">Category Name</a>
                                    </li>
                                    <li><a href="#">Category Name</a>
                                    </li>
                                    <li><a href="#">Category Name</a>
                                    </li>
                                    <li><a href="#">Category Name</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- /.row -->
                    </div>

                    <!-- Side Widget Well -->
                    <div class="well">
                        <h4>Side Widget Well</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis adipisci accusamus laudantium odit aliquam repellat tempore quos aspernatur vero.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="footer">
        <div class="go-top">
            <i class="fa fa-chevron-up icon" onclick="topFunction()" id="myBtn" title="Go to top"></i>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4">
                    <h4>Alamat Kami</h4>
                    <p>
                        Jl. Kliningan No.6,
                        <br/> +62 838 2252 3876,
                        <br/> Bandung, Indonesia.
                    </p>

                </div>
                <div class="col-lg-4">
                    <h4>Hubungi Kami</h4>
                    <i class="fa fa-twitter"></i>
                    <i class="fa fa-facebook"></i>
                    <i class="fa fa-google-plus"></i>
                </div>
                <div class="col-lg-4">
                    <h4>Lihat Kami!</h4>
                    <div id="googleMap" style="width:400px;height:150px;"></div>
                </div>
            </div>
        </div>
    </section>
    
      <div id="id01" class="modal">
        <div class="modal-dialog modal-lg" role="document">

            <form class="modal-content animate">
                <div class="modal-header" style="margin-bottom:30px;">
                    <h4 class="modal-title" id="exampleModalLabel">NEW SIMULATION</h4>
                    <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal" style="margin-top:8px;">&times;</span>
                </div>

        
                <form>
                    <div class="form-group">
                        <label class="control-label lbl">Username</label>
                        <input type="text" class="form-control" placeholder="Enter Username" required>
                    </div>

                    <div class="form-group">
                        <label class="control-label lbl">Select Simulation</label>
                        <select class="form-control select">
                            <option>- Pilih Simulation -</option>
                            <option></option>
                            <option></option>
                            <option></option>
                            <option></option>
                            <option></option>
                            <option></option>

                        </select>
                    </div>
                    

                    <div class="modal-footer" style="margin-top:30px;">
                        <button type="button" onclick="document.getElementById('id01').style.display='none'" class="btn btn-default">Cancel</button>
                        <button type="button" class="btn btn-primary">Go To Simulation</button>

                    </div>

                </form>
            </form>
        </div>
    </div>

    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.min.css">
    <script type="text/javascript" src="lib/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="lib/js/bootstrap.js"></script>
    <script type="text/javascript" src="lib/js/script.js"></script>
    <script type="text/javascript" src="lib/js/jquery-2.2.3.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA5XOldUyWaAqVd-EzPyHrV8e0mL3Fz1iY&callback=myMap"></script>
</body>

</html>