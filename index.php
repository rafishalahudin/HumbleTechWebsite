<?php 
        include 'koneksi/koneksi.php';
?>

    <html>

    <head>
        <title>Beranda - The Humble Tech</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="lib/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="lib/css/style.css">
    </head>

    <style>
        img.logo {
            width: 200px;
            height: 60px;
        }

    </style>

    <body>
        <div class="parallax">

            <nav id="mainNav" class="navbar navbar-fixed-top">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                        </button>
                        <div class="ikan"></div>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a class="page-scroll navigasi" href="index.php">Home</a>
                            </li>
                            <li>
                                <a class="page-scroll navigasi" href="ArtikelProc/category1.php">Laman Berita</a>
                            </li>
                            <li>
                                <a class="page-scroll navigasi" href="Event/categoryevent.php">Laman Event</a>
                            </li>
                            <li>
                                <a class="page-scroll navigasi" href="#contact" onclick="document.getElementById('id01').style.display='block'" style="width:auto;">Simulasi</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container-fluid -->
            </nav>


            <header>
                <div class="header-content">
                    <div class="header-content-inner">
                        <br>
                        <h1 id="homeHeader">Humble Tech</h1>
                        <p class="visi">Kami mengulas,kamu menentukan!</p>
                        <hr>
                        <p style="color:white; text-align:center;">Berita dan tempat untuk PC Builder!</p>

                    </div>
                </div>
            </header>

        </div>

        <div class="pilihan">
            <div class="col-sm-6">
                <img src="lib/image/GTX1080ti.png" style="max-width: 100%;">
            </div>
            <div class="col-sm-6">
                <h2>Website review berita yang akurat.</h2>
                <p>Kami mencari berita dan membuat berita yang akurat dengan penelitian yang benar benar baik serta tidak asal menyaring berita,dan juga dipastikan 100% berita yang kami sajikan adalah akurat.</p>
                <a href="ArtikelProc/category1.php" class="button" style="text-decoration:none;">Lihat berita!</a>
            </div>

        </div>
        <div class="pilihana">
            <div class="col-sm-6">
                <h2 class="a">Informasi tentang event teknologi.</h2>
                <p class="a">Menyajikan informasi tentang event event yang berhubungan dengan teknologi informasi yang ada pada saat ini yang diadakan oleh vendor yang terkenal dan besar.</p>
                <a href="Event/categoryevent.php" class="buttona" style="text-decoration:none;">Lihat berita!</a>
            </div>
            <div class="col-sm-6">
                <img src="lib/image/googleio.jpg" class="a">
            </div>

        </div>
        <div class="pilihanb">
            <div class="col-sm-12">
                <h2 class="b">Simulasi Rakit PC</h2>
            </div>
            <div class="col-sm-3">
                <h3>Budget</h3>
                <img src="lib/image/budget.png">
                <p class="a">Memperkirakan harga yang akurat dan tepat sekali untuk membuat/merakit PC idaman dengan kualitas yang baik tapi harga yang murah.</p>
            </div>
            <div class="col-sm-3">
                <h3>Skill</h3>
                <img src="lib/image/skill.png" width="180px" height="180px">
                <p class="a">Mempelajari bagaimana komponen komponen dari PC tersebut dapat terbuat,dan menjadi PC dengan rakitan yang sangat sempurna</p>
            </div>
            <div class="col-sm-3">
                <h3>Accuracy</h3>
                <img src="lib/image/tools.png" width="180px" height="180px">
                <p class="a">Mendapatkan komponen dengan kualitas yang terbaik diantara yang terbaik dengan kualitas yang baik dan yang harga yang sangat efektif.</p>
            </div>
            <div class="col-sm-3">
                <h3>Enjoy your PC</h3>
                <img src="lib/image/pc.png" width="180px" height="180px">
                <p class="a">Lalu nikmatilah PC rakitan mu dari simulasi yang sudah disediakan oleh Website ini dan nikmatilah game dengan settingan terultra dengan harga yang minimalis.</p>
            </div>
            <div class="col-sm-12">
                <a class="buttona" onclick="document.getElementById('id01').style.display='block'" style="text-decoration:none;">Rakit PC</a></div>

        </div>

        <div class="pilihanc">
            <div class="col-sm-12">
                <h2 class="c">Media Partner</h2>
            </div>
            <div class="col-sm-2">
                <img src="lib/image/m1.png" width="120px" height="70px">
            </div>
            <div class="col-sm-2">
                <img src="lib/image/m2.png" width="150px" height="90px">
            </div>
            <div class="col-sm-2">
                <img src="lib/image/m3.png" width="120px" height="90px">
            </div>
            <div class="col-sm-2">
                <img src="lib/image/m4.png" width="200px" height="100px">
            </div>
            <div class="col-sm-2">
                <img src="lib/image/m5.png" width="150px" height="100px">
            </div>
            <div class="col-sm-2" style="margin-bottom:30px">
                <img src="lib/image/m6.png" width="160px" height="100px">
            </div>
            <div class="col-sm-2">
                <img src="lib/image/m7.png" width="100px" height="90px">
            </div>
            <div class="col-sm-2">
                <img src="lib/image/m8.png" width="120px" height="120px">
            </div>
            <div class="col-sm-2">
                <img src="lib/image/m9.png" width="120px" height="85px">
            </div>
            <div class="col-sm-2">
                <img src="lib/image/m10.png" width="130px" height="100px">
            </div>
            <div class="col-sm-2">
                <img src="lib/image/m11.png" width="85px" height="100px">
            </div>
            <div class="col-sm-2">
                <img src="lib/image/m12.png" width="150px" height="150px">
            </div>
        </div>

        <div class="pilihand">
            <div class="col-sm-12">
                <h2 class="c">Masukan untuk web ini</h2>
            </div>
            <form action="proses.php" method="post">
                <div class="col-sm-6">
                    <input type="text" class="a" placeholder="NAMA" name="nama" required>
                    <input type="text" class="a" placeholder="PHONE" name="phone" required>
                </div>
                <div class="col-sm-6">
                    <input type="text" class="a" placeholder="EMAIL" name="email" required>
                    <input type="text" class="a" placeholder="ALAMAT" name="alamat" required>
                </div>
                <div class="col-sm-12">
                    <textarea class="b" placeholder="Contoh : Perbaiki bagian home nya,kurang enak dilihat" name="pesan" required></textarea>
                    <button type="submit" style="margin-top: 30px;" class="buttona">Submit!</button>
                </div>
            </form>


            <div class="col-sm-3" style="margin-top:30px">
                <img src="lib/image/ig.png" width="50px" height="50px">
                <p style="font-weight: 600; text-transform:uppercase">HumbleTech</p>

            </div>
            <div class="col-lg-6" style="margin-top:30px;">

                <img src="lib/image/pin.png" width="50px" height="50px">
                <p style="font-weight: 600; text-transform:uppercase">SMK NEGERI 4 BANDUNG</p>

            </div>
            <div class="col-sm-3" style="margin-top:30px"><img src="lib/image/phone.png" width="50px" height="50px">
                <p style="font-weight: 600; text-transform:uppercase">0838-2252-3876</p>
            </div>
            <div class="col-sm-12">
                <div id="googleMap" class="map"></div>
            </div>

        </div>


        <div class="pilihane">
            <div class="col-sm-12">
                <h4 class="e">&copy Deffin-Iqbal-Rafi || <a class="form-signin-heading" style="color:white;" href="login.php">Login</a></h4>
                
            </div>
        </div>

        <div id="id01" class="modal">
            <div class="modal-dialog modal-lg" role="document">

                <form class="modal-content animate" action="reportsimulasi.php" method="post">
                    <div class="modal-header" style="margin-bottom:30px;">
                        <h4 class="modal-title" id="exampleModalLabel">SIMULASI BARU</h4>
                        <span onclick="document.getElementById('id01').style.display='none'" class="close" title="Close Modal" style="margin-top:8px;">&times;</span>
                    </div>


                    <form action="reportsimulasi.php" method="post">
                        <div class="form-group">
                            <label class="control-label lbl">Masukan Nomer rakitan</label>
                            <input type="text" name="id_rakit" class="form-control" placeholder="Masukan nomor Rakitan" required>
                        </div>

                        <div class="form-group">
                            <label class="control-label lbl">Pilih Processor</label>
                            <select class="form-control select" name="id_processor">
                                <?php $dataProc = mysql_query("SELECT * FROM t_processor") ?>
                                    <?php while($row = mysql_fetch_object($dataProc)){
    ?>

                                        <option value="<?php echo $row->id_processor?>">
                                            <?php echo @$result->id_processor == $row->id_processor ? 'selected' : '' ?>
                                                <?php echo $row->nama_processor."  (Rp.".$row->hargapro.")"?>
                                        </option>
                                        <?php } ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="control-label lbl">Pilih Video Graphics Array</label>
                            <select class="form-control select" name="id_vga">
                                <?php $dataVga = mysql_query("SELECT * FROM t_vga") ?>
                                    <?php while($row = mysql_fetch_object($dataVga)){
    ?>

                                        <option value="<?php echo $row->id_vga?>">
                                            <?php echo @$result->id_vga == $row->id_vga ? 'selected' : '' ?>
                                                <?php echo $row->nama_vga."  (Rp.".$row->hargavga.")"?>
                                        </option>
                                        <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label lbl">Pilih Motherboard</label>
                            <select class="form-control select" name="id_mobo">
                                <?php $datamobo = mysql_query("SELECT * FROM t_mobo") ?>
                                    <?php while($row = mysql_fetch_object($datamobo)){
    ?>

                                        <option value="<?php echo $row->id_mobo?>">
                                            <?php echo @$result->id_mobo == $row->id_mobo ? 'selected' : '' ?>
                                                <?php echo $row->nama_mobo."  (Rp.".$row->hargamobo.")"?>
                                        </option>
                                        <?php } ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="control-label lbl">Pilih Ram</label>
                            <select class="form-control select" name="id_ram">
                                <?php $dataram = mysql_query("SELECT * FROM t_ram") ?>
                                    <?php while($row = mysql_fetch_object($dataram)){
    ?>

                                        <option value="<?php echo $row->id_ram?>">
                                            <?php echo @$result->id_ram == $row->id_ram ? 'selected' : '' ?>
                                                <?php echo $row->nama_ram."  (Rp.".$row->hargaram.")"?>
                                        </option>
                                        <?php } ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="control-label lbl">Pilih Power Supply</label>
                            <select class="form-control select" name="id_powersuply">
                                <?php $datapsu = mysql_query("SELECT * FROM t_powersuply") ?>
                                    <?php while($row = mysql_fetch_object($datapsu)){
    ?>

                                        <option value="<?php echo $row->id_psu?>">
                                            <?php echo @$result->id_psu == $row->id_psu ? 'selected' : '' ?>
                                                <?php echo $row->nama_psu."  (Rp.".$row->hargapsu.")"?>
                                        </option>
                                        <?php } ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="control-label lbl">Pilih Penyimpanan</label>
                            <select class="form-control select" name="id_hdd">
                                <?php $datahdd = mysql_query("SELECT * FROM t_harddisk") ?>
                                    <?php while($row = mysql_fetch_object($datahdd)){
    ?>

                                        <option value="<?php echo $row->id_hdd?>">
                                            <?php echo @$result->id_hdd == $row->id_hdd ? 'selected' : '' ?>
                                                <?php echo $row->nama_hdd."  (Rp.".$row->hargahdd.")"?>
                                        </option>
                                        <?php } ?>
                            </select>
                        </div>


                        <div class="modal-footer" style="margin-top:30px;">
                            <button type="button" onclick="document.getElementById('id01').style.display='none'" class="btn btn-default">Cancel</button>
                            <button type="submit" class="btn btn-primary">Go To Simulation</button>

                        </div>

                    </form>
                </form>
            </div>
        </div>

        <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.min.css">
        <script type="text/javascript" src="lib/js/jquery-2.2.3.js"></script>
        <script type="text/javascript" src="lib/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="lib/js/bootstrap.js"></script>
        <script type="text/javascript" src="lib/js/script.js"></script>
        <script type="text/javascript" src="lib/js/scrolltop.js"></script>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA5XOldUyWaAqVd-EzPyHrV8e0mL3Fz1iY&callback=myMap"></script>



    </body>

    </html>
