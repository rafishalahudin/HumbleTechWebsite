<!DOCTYPE html>
<html lang="en">

<head>

    <title>Humble Tech - Admin</title>

    <link href="../lib/css/bootstrap.min.css" rel="stylesheet">
    <link href="../lib/css/sb-admin.css" rel="stylesheet">
    <link href="../lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Humble Tech</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav" style="background-color: #222">


                <li class="dropdown" >
                    <a href="#"  class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> Rafi Shalahudin <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="../logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#simu"><i class="fa fa-fw fa-table"></i> Database Simulasi <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="simu" class="collapse">
                           <li>
                                <a href="../Processor/dataproc.php">Processor</a>
                            </li>
                            <li>
                                <a href="../Motherboard/datamobo.php">Motherboard</a>
                            </li>
                            <li>
                                <a href="../Vga/datavga.php">VGA</a>
                            </li>
                            <li>
                                <a href="../Ram/dataram.php">RAM</a>
                            </li>
                            <li>
                                <a href="../Harddisk/datahdd.php">Hard Disk Drive</a>
                            </li>
                            <li>
                                <a href="../Powersuply/datapsu.php">Power Supply</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#art"><i class="fa fa-fw fa-table"></i> Database Artikel <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="art" class="collapse">
                           <li>
                                <a href="../ArtikelProc/datakonten.php">Artikel Processor</a>
                            </li>
                            <li>
                                <a href="../Event/dataevent.php">Artikel Events</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-table"></i> Form Simulasi <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="../Processor/tambahproc.php">Processor</a>
                            </li>
                            <li>
                                <a href="../Motherboard/tambahmobo.php">Motherboard</a>
                            </li>
                            <li>
                                <a href="../Vga/tambahvga.php">VGA</a>
                            </li>
                            <li>
                                <a href="../Ram/tambahram.php">RAM</a>
                            </li>
                            <li>
                                <a href="../Harddisk/tambahhdd.php">Hard Disk Drive</a>
                            </li>
                            <li>
                                <a href="../Powersuply/tam">Power Supply</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#ha"><i class="fa fa-fw fa-arrows-v"></i> Form Artikel <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="ha" class="collapse">
                            <li>
                                <a href="../simulasi/datasimulasi.php">Simulasi</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <center>
                        <h1>Masukan data Powersuply</h1></center>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">

                                <form action="tambahpsu.php" method="post">

                                    <fieldset>
                                        <div class="form-group col-md-6">
                                            <label for="first_name">Id Powersuply</label>
                                            <input type="text" name="id_psu" class="form-control" placeholder="Masukan Id Powersuply" required>
                                        </div>

                                        <div class="form-group col-md-6">
                                            <label for="first_name">Nama Powersuply</label>
                                            <input type="text" name="nama_psu" class="form-control" placeholder="Masukan Nama Powersuply" required>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="first_name">Harga Powersuply</label>
                                            <input type="text" name="hargapsu" class="form-control" placeholder="Masukan Harga Powersuply" required>
                                        </div>
                                    </fieldset>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <button type="submit" value="simpan" class="btn btn-primary">
                                                Submit
                                            </button>

                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                    <script src="../lib/media/js/jquery.min.js"></script>
                    <script src="../lib/media/js/bootstrap.min.js"></script>
                    <script src="../lib/media/plugins/toastr/toastr.min.js"></script>
                    <link href="../lib/media/plugins/toastr/toastr.min.css" rel="stylesheet">
                </div>
            </div>
        </div>
    </div>
</body>

</html>